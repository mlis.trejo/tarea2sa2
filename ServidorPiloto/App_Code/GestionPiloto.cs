﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de GestionRestaurante
/// </summary>
public class GestionRestaurante
{
    // Declaracion de variables globales

    // Declaracion de lista con todos los Restaurantes disponibles
    private LinkedList<EntidadRestaurante> _listaRestaurantes;
    // Declaracion de un random, por motivos del demo
    Random _rnd = new Random();


    /*
    * Constructor para la gestion de Restaurantes
    * Parametros: ninguno
    * nstancia la lista de Restaurantes y para motivos de la simulacion se insertan 8 Restaurantes
    */
    public GestionRestaurante() {
        _listaRestaurantes = new LinkedList<EntidadRestaurante>();
        AgregarRestaurante(new EntidadRestaurante("123ABC", EntidadRestaurante.Tipo_Estado.DISPONIBLE, 1,"Julio Arango"));
        AgregarRestaurante(new EntidadRestaurante("456DEF", EntidadRestaurante.Tipo_Estado.DISPONIBLE, 2, "Julia Sierra"));
        AgregarRestaurante(new EntidadRestaurante("789GHI", EntidadRestaurante.Tipo_Estado.DISPONIBLE, 3, "Alba Chinchilla"));
        AgregarRestaurante(new EntidadRestaurante("123JKL", EntidadRestaurante.Tipo_Estado.DISPONIBLE, 4, "Gustavo Cruz"));
        AgregarRestaurante(new EntidadRestaurante("456MNO", EntidadRestaurante.Tipo_Estado.DISPONIBLE, 5, "Daniel Garcia"));
        AgregarRestaurante(new EntidadRestaurante("789PQR", EntidadRestaurante.Tipo_Estado.DISPONIBLE, 6, "Juan Carlos"));
        AgregarRestaurante(new EntidadRestaurante("124STU", EntidadRestaurante.Tipo_Estado.DISPONIBLE, 7, "Lupita Godinez"));
        AgregarRestaurante(new EntidadRestaurante("456VWX", EntidadRestaurante.Tipo_Estado.DISPONIBLE, 8, "Edgar Arriola"));
    }

    /*
    * Metodo para agregar nuevos pedidos para el Restaurante 
    * Parametros: *EntidadRestaurante, nodo que contiene todos los datos del pedido
    * agrega el Restaurante mandado a la lista de Restaurantes
    */
    public void AgregarRestaurante(EntidadRestaurante Restaurante) {
        _listaRestaurantes.AddLast(Restaurante);
    }


    public String ObtenerInformacion(string placa) {

        foreach (EntidadRestaurante Restaurante in _listaRestaurantes)
        {
            if (Restaurante.GetPlaca() == placa)
            {
                return "Nombre Restaurante: " + Restaurante.GetNombre() + "\n" +
                    "Placa Carro: " + Restaurante.GetPlaca() + "\n" +
                    "Zona Actual: " + Restaurante.GetZonaCubierta() + "\n" +
                    "Tiempo Estimado: " + _rnd.Next(1, 15); 
            }
        }
        return "Sin Restaurantes disponibles";
    }

    /*
    * Metodo para el cambio de estado del Restaurante
    * Parametros: *codigo    --> entero que contiene el codigo del Restaurante que se desea modificar
    * Recorre todos los Restaurantes y cambia el necesario
    */
    public void OcuparRestaurante(int codigo) 
    {
        // Ciclo iterador para recorrer todos los Restaurantes disponibles
        foreach (EntidadRestaurante Restaurante in _listaRestaurantes)
        {
           
        }
    }
}